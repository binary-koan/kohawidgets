function getTimeCounts(fn) {
  $.getJSON('http://opac.koha.workbuffer.org/cgi-bin/koha/svc/report?id=15', function(data) {
    var to_plot = [["12am-3am", 0], ["3am-6am", 0], ["6am-9am", 0], ["9am-12pm", 0], ["12pm-3pm", 0], ["3pm-6pm", 0], ["6pm-9pm", 0], ["9pm-12pm", 0]];
    $.each(data, function(i) {
      switch(data[i][1]) {
        case "0":
        case "1":
        case "2":
          to_plot[0][1] += parseInt(data[i][0]);
          break;
        case "3":
        case "4":
        case "5":
          to_plot[1][1] += parseInt(data[i][0]);
          break;
        case "6":
        case "7":
        case "8":
          to_plot[2][1] += parseInt(data[i][0]);
          break;
        case "9":
        case "10":
        case "11":
          to_plot[3][1] += parseInt(data[i][0]);
          break;
        case "12":
        case "13":
        case "14":
          to_plot[4][1] += parseInt(data[i][0]);
          break;
        case "15":
        case "16":
        case "17":
          to_plot[5][1] += parseInt(data[i][0]);
          break;
        case "18":
        case "19":
        case "20":
          to_plot[6][1] += parseInt(data[i][0]);
          break;
        case "21":
        case "22":
        case "23":
          to_plot[7][1] += parseInt(data[i][0]);
          break;
      }
    });
    fn(to_plot);
  });
}

function getDayCounts(fn) {
  $.getJSON('http://opac.koha.workbuffer.org/cgi-bin/koha/svc/report?id=20', function(data) {
    var to_plot = [["Sunday", 0], ["Monday", 0], ["Tuesday", 0], ["Wednesday", 0], ["Thursday", 0], ["Friday", 0], ["Saturday", 0], ["Sunday", 0]];
    window.years = [];
    window.months = [];
    window.days = [];
    window.data = data;
    window.d = [];
    $.each(data, function(i) {
      var d = data[i][1];
      window.d.push(d);
      var year = parseInt(d.substring(0,4)), month = parseInt(d.substring(5,7)), day = parseInt(d.substring(8,10));
      var date = new Date(year, month, day);
      window.years.push(year);
      window.months.push(month);
      window.days.push(date.getDay());
      switch(date.getDay()) {
        case 0:
          to_plot[0][1] += parseInt(data[i][0]);
          break;
        case 1:
          to_plot[1][1] += parseInt(data[i][0]);
          break;
        case 2:
          to_plot[2][1] += parseInt(data[i][0]);
          break;
        case 3:
          to_plot[3][1] += parseInt(data[i][0]);
          break;
        case 4:
          to_plot[4][1] += parseInt(data[i][0]);
          break;
        case 5:
          to_plot[5][1] += parseInt(data[i][0]);
          break;
        case 6:
          to_plot[6][1] += parseInt(data[i][0]);
          break;
      }
    });
    fn(to_plot);
  });
}

var jqplotOptions = {
    series:[{renderer:$.jqplot.BarRenderer}],
    axes:{
      xaxis:{
        renderer:$.jqplot.CategoryAxisRenderer,
        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
        tickOptions: {
          angle: -30,
          fontSize: '10pt'
        },
        pad: 0
      },
      yaxis:{
      }
    },
    highlighter: {
      show: true,
      tooltipAxes: 'y'
    },
    cursor: {
      show: false
    }
}

function switchToDay() {
  $('#wdayimg').hide();
  $('#wtimeimg').show();
  $('#returnschart').html("");
  $('#wreturnstitle').html("Total Returns by weekday");
  getDayCounts(function(to_plot) {
    var plot = $.jqplot('returnschart', [to_plot], jqplotOptions);
  });
}

function switchToTime() {
  $('#wtimeimg').hide();
  $('#wdayimg').show();
  $('#returnschart').html("");
  $('#wreturnstitle').html("Total Returns by time of day");
  getTimeCounts(function(to_plot) {
    var plot = $.jqplot('returnschart', [to_plot], jqplotOptions);
  });
}

$(function() {
  switchToTime();
});